package lambdaStream.optional;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;


class Insurance { private String name;
	public String getName() { return name; }
}

class Car {
	private Optional<Insurance> insurance;
	public Optional<Insurance> getInsurance() { 
		return insurance;
	}
}

public class Person {
	private Optional<Car> car;
	public Optional<Car> getCar() { return car; }
	
	
	public static String getCarInsuranceName(Optional<Person> p) {
		return p.flatMap(person -> person.getCar())
				.flatMap(car -> car.getInsurance())
				.map(insurance -> insurance.getName())
				.orElse("Unknown");
		}
	
	
	public static final void main(String args[]) {
		Supplier<Optional<Person>> supp = Optional::empty;
	
		getCarInsuranceName(supp.get());
	}
}


