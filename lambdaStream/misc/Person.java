package lambdaStream.misc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Person {

	private int age;
	private String name;
	
	public Person(int age, String name) {
		this.age = age;
		this.name = name;
	}
	
	public int getAge() {return age;}
	public String getName() {return name;}
	
	@Override
	public String toString() {return age + ":" + name;}

	public static final void main(String args[]) {

		List<Person> people = new ArrayList<>();
		people.add(new Person(18, "B"));
		people.add(new Person(18, "A"));		
		
		
		Collections.sort(people, Comparator.comparing(Person::getAge)
				.thenComparing(Person::getName));
		
		System.out.println(people);
		
		Collections.sort(people, (p1, p2) -> p1.age - p2.age);
	}
}
