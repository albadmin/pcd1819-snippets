package lambdaStream.misc;

import java.util.stream.LongStream;

class Accumulator {
	public long total = 0;
	public void add(long value) { total += value; }
}

public class ParalleSideEffect {

	public static long sideEffectSequentialSum(long n) {
		Accumulator accumulator = new Accumulator();
		LongStream.rangeClosed(1, n).forEach(accumulator::add);
		return accumulator.total;
	}

	public static long sideEffectParallelSum(long n) {
		Accumulator accumulator = new Accumulator();
		LongStream.rangeClosed(1, n).parallel().forEach(accumulator::add);
		return accumulator.total;
	}
	
	public static final void main(String args[]) {
		for(int i=0; i<5; i++) {
			System.out.println(sideEffectSequentialSum(100));			
		}
		System.out.println("---------------------------------");
		for(int i=0; i<5; i++) {
			System.out.println(sideEffectParallelSum(100));
		}
	}
}
