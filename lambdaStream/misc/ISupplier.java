package lambdaStream.misc;

import java.util.function.*;

public class ISupplier {

	public static final void main(String args[]) {
		Supplier<String> noArg  = String::new;//Reference to a constructor
		Supplier<String> noArg1  = () -> new String();
		
		Function<String, String> singleArg = String::new;
		Function<String, String> singleArg1 = s -> new String(s);		
		
		Consumer<String> cons = System.out::println;
		cons.accept(noArg.get());
		cons.accept(noArg1.get());
	
		cons.accept(singleArg.apply("PCD"));
		cons.accept(singleArg1.apply("1819"));
				
	    BiFunction<String, String,String> bi = (x, y) -> {return x + y;};
	    
		cons.accept(bi.apply("PCD", "1819"));
	}
}
