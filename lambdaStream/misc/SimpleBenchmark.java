package lambdaStream.misc;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.*;

class UUIDPerson {
	
	private static Random rand = new Random(System.currentTimeMillis());
	
	private int identifier;
	private double salary;
	
	public UUIDPerson(int identifier) {
		this.identifier = identifier;
		this.salary = 1000 + (2000 - 1000) * rand.nextDouble();
	}
	
	public int getUUID() {return identifier;}
	
	public void setSalary(double increment) {
		salary+=salary*increment;
	}
	
	public double getSalary() {return salary;}
	
	public String toString() {
		return "Id(" + identifier + ")" + " Salary(" + salary+ ")";
	}
	
}

public class SimpleBenchmark {

	public static List<UUIDPerson> persons = null;
	public static Random rand = new Random(System.currentTimeMillis());
	public static int MAX_BOUND = 1000;
	
	static{	
		
		Supplier<Integer> next =  () -> rand.nextInt(MAX_BOUND);
/*		
		persons = ThreadLocalRandom.current()
						 .ints(0, 100)
						 .filter(x->x%2 ==0)
						 .distinct()
						 .limit(MAX_BOUND/10)
						 .mapToObj(UUIDPerson::new)
						 .collect(Collectors.toList());
*/						 
		IntSupplier supp = () -> rand.nextInt(MAX_BOUND);
		persons = IntStream.generate(supp)
				.filter(x-> x%2 ==0)
				.distinct()
				.limit(MAX_BOUND/10)
				.mapToObj(UUIDPerson::new)
				.collect(Collectors.toList());
/*
		Supplier<Integer> supp1 = () -> rand.nextInt(MAX_BOUND);
		persons = Stream.generate(supp1)
						.filter(x-> x%2 ==0)
						.distinct()
						.limit(MAX_BOUND/10)
						.mapToInt(Integer::intValue)
						.mapToObj(UUIDPerson::new)
						.collect(Collectors.toList());		
*/
	}
	
	
	public static final void main(String args[]) {

		System.out.println("Generated a total of " + persons.size() + " persons");
		long start = System.nanoTime();
		persons.forEach(p->p.setSalary(0.1));
		long end = System.nanoTime();
		
		System.out.println("Serial Stream computation took: " + (double)(end-start)/1_000_000 + " ms");
		
		start = System.nanoTime();
		persons.parallelStream().forEach(p->p.setSalary(0.1));
		end = System.nanoTime();
		System.out.println("Parallel Stream computation took: " + (double)(end-start)/1_000_000 + " ms");		
		
		start = System.nanoTime();
		for(UUIDPerson p: persons) 
			p.setSalary(0.1);
		end = System.nanoTime();
		
		System.out.println("External computation took: " + (double)(end-start)/1_000_000 + " ms");
		
		//calculations: max salary
		UUIDPerson max = Collections.max(persons, (p1, p2) -> 
				Double.compare(p1.getSalary(), p2.getSalary()));
		System.out.println(max);
		
	}	
}
