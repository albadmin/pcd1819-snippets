package nio.clientServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
 
public class ThreadedClient extends Thread {
 
	private static final String LOG_ERR = "err";
	private static final String LOG_OUT = "out";

	private int identifier;
	
	
	public ThreadedClient(int identifier) {
		this.identifier = identifier;
	}
	
	public void run() {
 
		InetSocketAddress remoteAddr = new InetSocketAddress("localhost", 1111);
		SocketChannel client;
		try {
			client = SocketChannel.open(remoteAddr);
		} catch (IOException e) {
			log(identifier, " Error opening socket", LOG_ERR);
			return;
		}
 
		log(identifier, "Connecting to Server on port 1111...", LOG_OUT);
 
		ArrayList<String> messageList = new ArrayList<String>();
		
		// create a ArrayList with companyName list
		messageList.add("Hi from");
		messageList.add("PCD1819");
		messageList.add("written");
		messageList.add("by");
		messageList.add(Thread.currentThread().getName());
		messageList.add("close");//message used to denote end-of-session
		
		for (String companyName : messageList) {
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			ObjectOutputStream oos = new ObjectOutputStream (baos);
//			oos.writeObject (myObjectToSerialize);
//			oos.flush();
//			channel.write (ByteBuffer.wrap (baos.toByteArray()));

			byte[] message = new String(companyName).getBytes();
			ByteBuffer buffer = ByteBuffer.wrap(message);
			try {
				client.write(buffer);
			} catch (IOException e) {
				log(identifier, "Error writing packet " + companyName, LOG_ERR);
			}
 
			log(identifier, "sending: " + companyName, LOG_OUT);
			buffer.clear();
 
			// wait for 2 seconds before sending next message
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			client.close();
		} catch (IOException e) {
			log(identifier, "Error while closing connection with the server", LOG_ERR);
		}
	}
 
	private static void log(int flowIdentifier, String str, String mode) {
		switch(mode) {
			case LOG_OUT: {
				System.out.println(flowIdentifier + " " + str); 
			}
			case LOG_ERR: {
				System.err.println(flowIdentifier + " " + str); 
			}
		}
	}
	
	public static final void main(String args[]) {
		
		for(int i=0; i<10; i++) {
			new ThreadedClient(i).start();
		}
	}
}
