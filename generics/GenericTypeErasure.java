package theme3.generics;

public class GenericTypeErasure<K, V> {
    
	private K key;
    private V value;
    
    public GenericTypeErasure(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() { return key; }
    public V getValue() { return value; }

    public void setKey(K key)     { this.key = key; }
    public void setValue(V value) { this.value = value; }


    public static final void main(String args[]) {
    	
    	GenericTypeErasure<Integer, Integer> p1 = new GenericTypeErasure<>(1, 2);
    	GenericTypeErasure<String, String> p2= new GenericTypeErasure<>("1", "2");
    	System.out.println(p1.getClass().getName() == p2.getClass().getName());
    	System.out.println(p1.getClass().getName());
    }
}