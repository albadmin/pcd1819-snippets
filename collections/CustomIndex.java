package theme3.collections;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class MyCustomMapIndex {
	
	private final int primary_value;
	
	public MyCustomMapIndex(int value) {
		primary_value = value;
	}	

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!(obj instanceof MyCustomMapIndex)) return false;
		MyCustomMapIndex other = (MyCustomMapIndex)obj;
		return primary_value == other.primary_value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(primary_value);
	}
/*
	@Override
	public int hashCode() {
	    int hash = 7;
	    hash = 31 * hash + (int) id;
	    hash = 31 * hash + (name == null ? 0 : name.hashCode());
	    hash = 31 * hash + (email == null ? 0 : email.hashCode());
	    return hash;
	}
*/	
}

public class CustomIndex {

	public static final void main(String args[]) {
		
		Map<MyCustomMapIndex, List<Integer>> map = new HashMap<>();
		MyCustomMapIndex v1 = new MyCustomMapIndex(2);
		List<Integer> under_v1 = Stream.of(1, 2, 3, 10).collect(Collectors.toList());
		map.put(v1, under_v1);
		
		MyCustomMapIndex v2 = new MyCustomMapIndex(2);
		List<Integer> under_v2 = Stream.of(10, 20, 30, 100).collect(Collectors.toList());
		map.put(v2, under_v2);		

		System.out.println(map.size());
	}
}