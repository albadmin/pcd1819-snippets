package theme3.IOEx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class PreJSE7StreamClose {
    public static void main(String[] args) {

    	readVersion1PreJSE7();
    	readVersionPostJSE7();
    }
    
    public static void readVersion1PreJSE7() {
    	FileInputStream stream = null;
        try {
            System.out.println("First statement");
            File file = new File("pippo.txt");
            stream = new FileInputStream(file);
            stream.read();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Inside the finally block");
        }
        System.out.println("After catch blocks");
    }
    
    public static void readVersionPostJSE7() {

    	System.out.println("First statement");
        File file = new File("pippo.txt");
        try (FileInputStream stream = new FileInputStream(file)){
        } catch (IOException e) {}

        System.out.println("After catch blocks");
    }
}