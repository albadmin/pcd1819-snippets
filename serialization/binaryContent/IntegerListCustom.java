package serialization.binaryContent;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Random;

public class IntegerListCustom implements Serializable {
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 3561795905886621241L;

	transient private Node head;
    transient private Node tail;
    transient private int size;
     
    public IntegerListCustom() {
        size = 0;
    }
    /**
     * this class keeps track of each element information
     * @author java2novice
     *
     */
    private class Node {
        Integer element;
        Node next;
        Node prev;
 
        public Node(Integer element, Node next, Node prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }
    }
    /**
     * returns the size of the linked list
     * @return
     */
    public int size() { return size; }
     
    /**
     * return whether the list is empty or not
     * @return
     */
    public boolean isEmpty() { return size == 0; }
     
    /**
     * adds element at the starting of the linked list
     * @param element
     */
    public void addFirst(Integer element) {
        Node tmp = new Node(element, head, null);
        if(head != null ) {head.prev = tmp;}
        head = tmp;
        if(tail == null) { tail = tmp;}
        size++;
    }
     
    /**
     * adds element at the end of the linked list
     * @param element
     */
    public void addLast(Integer element) {
         
        Node tmp = new Node(element, null, tail);
        if(tail != null) {tail.next = tmp;}
        tail = tmp;
        if(head == null) { head = tmp;}
        size++;
    }
     
    /**
     * this method walks forward through the linked list
     */
    public void iterateForward(){
         
        System.out.println("iterating forward..");
        Node tmp = head;
        while(tmp != null){
            System.out.println(tmp.element);
            tmp = tmp.next;
        }
    }
     
     
    /**
     * this method removes element from the start of the linked list
     * @return
     */
    public Integer removeFirst() {
        if (size == 0) throw new NoSuchElementException();
        Node tmp = head;
        head = head.next;
        head.prev = null;
        size--;
        return tmp.element;
    }
     
    /**
     * this method removes element from the end of the linked list
     * @return
     */
    public Integer removeLast() {
        if (size == 0) throw new NoSuchElementException();
        Node tmp = tail;
        tail = tail.prev;
        tail.next = null;
        size--;
        return tmp.element;
    }
    
    private void writeObject(ObjectOutputStream out) throws IOException {
    	out.defaultWriteObject();

    	out.writeInt(size);
    	
        Node tmp = head;
        while(tmp != null){
        	out.writeObject(tmp.element);
            tmp = tmp.next;
        }    	
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    	in.defaultReadObject();
    	int numElements = in.readInt();
    	System.out.println("Should deserialize a total of: " + numElements + " list elements");
    	
    	for(int i=0; i<numElements; i++) {
    		addFirst((Integer) in.readObject());
    	}
    }
     
    public static void main(String a[]){
         
        IntegerListCustom sDll = new IntegerListCustom();
        Random rand = new Random(System.currentTimeMillis());
        for(int i=0; i < 1000; i++) {
        	sDll.addFirst(rand.nextInt(1000));
        }
        byte[] rawContent = null;
		try(ByteArrayOutputStream rawb = new ByteArrayOutputStream(); ObjectOutputStream  objWriter = new ObjectOutputStream(rawb)) {
			objWriter.writeObject(sDll);
			rawContent = rawb.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Object serialized form weight: " + rawContent.length);
		
		try(ByteArrayInputStream rawb = new ByteArrayInputStream(rawContent); ObjectInputStream  objReader = new ObjectInputStream(rawb)) {
			
			objReader.readObject();			
		} catch (IOException | ClassNotFoundException cce) {
			// TODO Auto-generated catch block
			cce.printStackTrace();
		}
    }
}
