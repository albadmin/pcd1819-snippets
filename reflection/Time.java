package theme2;

public final class Time {
	private static final int HOURS_PER_DAY = 24;
	private static final int MINUTES_PER_HOUR = 60;

	public final int hour;
	public final int minute;

	// But we can check invariants, since fields are final
	public Time ( int hour, int minute ) {
			if (hour < 0 || hour >= HOURS_PER_DAY)
				throw new IllegalArgumentException();
			if (minute < 0 || minute >= MINUTES_PER_HOUR)
				throw new IllegalArgumentException();
			
			this.hour = hour;
			this.minute = minute;
	}
}