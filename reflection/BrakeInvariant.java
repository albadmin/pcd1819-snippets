package theme2;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class BrakeInvariant {

	static void setFinalStatic(Object timeObj, Field field, Object newValue) throws Exception {

		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field,
		field.getModifiers() & ~Modifier.FINAL);
		field.set(timeObj, newValue);
	}

	//System.setSecurityManager(new SecurityManager());
	//java -Djava.security.manager=your.custom.securityManager YouApp
	//java -Djava.security.manager -Djava.security.policy=examplepolicy
	public static void main(String args[]) throws Exception {
		Time today = new Time(11, 0);
		System.setSecurityManager(new SecurityManager());
		setFinalStatic(today, today.getClass().getField("hour"), 25);
		System.out.printf("Hour of day: %d", today.hour);
	}
}
